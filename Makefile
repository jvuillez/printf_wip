# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/04/19 23:42:33 by jvuillez          #+#    #+#              #
#    Updated: 2016/04/04 13:56:47 by jvuillez         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

CFLAGS = -Wall -Werror -Wextra

NAME = libftprintf.a

OBJ_O = ft_printf.o \
		ft_printspe.o \
		ft_print_nbr.o \
		ft_parser.o \
		ft_tools_common.o \
		ft_tools_len.o \
		ft_tools_put.o

all: $(NAME)

$(NAME): $(OBJ_O)
	ar rc $(NAME) $(OBJ_O)
	ranlib $(NAME)

%.o: %.c
	$(CC) -o $@ -c $^ $(CFLAGS)

clean:
	rm -f $(OBJ_O)

fclean: clean
	rm -f $(NAME)

re: fclean all
