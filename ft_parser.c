/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 17:05:43 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 19:18:28 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

/*
 **	%[flags][width][.precision][length]specifier
 **
 **	%[flags]		-	;	+	;	~space	;	#	;	0
 **	[width]			~number		;	*
 **	[.precision]	~.number	;	.*
 **	[length]		modify the specifier
 **					hh	;	h	;	l	;	ll	;	j	;	z	;	t	;	L
 **		42			hh	;	h	;	l	;	ll	;	j	;	z
 ** specifier	% s   p d   i o   u   x X c   f F e E g G a A n
 ** specifier42	% s S p d D i o O u U x X c C
 */

int				ft_flags(char *str, t_format *f)
{
	static char	flags[] = "-+ #0";
	static int	flag_value[] = {1, 2, 4, 8, 16};
	int			i;
	int			j;

	f->f = 0;
	f->flag = 0;
	i = 0;
	j = 0;
	while (j != 5)
	{
		j = 0;
		while (j != 5 && flags[j] != str[i])
			j++;
		if (j != 5 && (f->flag & flag_value[j]) == 0)
			f->flag += flag_value[j];
		i++;
	}
	if (f->flag != 0)
		f->f = 1;
	return (i - 1);
}

int				ft_width(char *str, t_format *f, va_list ap)
{
	int		i;

	i = 0;
	f->w = 0;
	f->width = 0;
	if (str[i] >= '1' && str[i] <= '9')
	{
		f->w = 1;
		f->width = ft_atoi(&str[i]);
		i += ft_nb_length(ft_atoi(&str[i]), "0123456789");
	}
	if (str[i] == '*')
	{
		f->w = 1;
		f->width = va_arg(ap, int);
		i++;
		if ((f->flag & MOINS) == 1 && f->width < 0)
		{
			f->flag -= 1;
			f->width *= -1;
		}
	}
	return (i);
}

int				ft_precision(char *str, t_format *f, va_list ap)
{
	int		i;

	i = 0;
	f->p = 0;
	f->precision = 0;
	if (str[i] == '.')
	{
		f->p = 1;
		i++;
		if (str[i] >= '0' && str[i] <= '9')
		{
			f->precision = ft_atoi(&str[i]);
			i += ft_nb_length(ft_atoi(&str[i]), "0123456789");
		}
		if (str[i] == '*')
		{
			f->precision = va_arg(ap, int);
			i++;
		}
	}
	return (i);
}

int				ft_length(char *str, t_format *f)
{
	static char	lengths[] = "hljz";
	int			j;

	j = 0;
	f->length = (char*)malloc(sizeof(char) * 3);
	while (j != 4 && str[0] != lengths[j])
		j++;
	if (j != 4)
	{
		f->length[0] = lengths[j];
		if (str[1] == str[0])
		{
			f->length[1] = lengths[j];
			f->length[2] = '\0';
			return (2);
		}
		else
		{
			f->length[1] = '\0';
			return (1);
		}
	}
	free(f->length);
	f->length = NULL;
	return (0);
}

int				ft_parser(char *str, va_list ap)
{
	void		(*tab_ft[127])(void *data, t_format *f);
	void		*data;
	t_format	f;

	str += ft_flags(str, &f);
	str += ft_width(str, &f, ap);
	str += ft_precision(str, &f, ap);
	str += ft_length(str, &f);
	f.sp = (int)*str;
	f.tot = 0;
	if (ft_specifier(f.sp) == -1)
		f.tot += ft_putstr(str);
	else
	{
		ft_init_tab_ft(tab_ft);
		data = va_arg(ap, void*);
		tab_ft[f.sp](data, &f);
	}
	return (f.tot);
}
