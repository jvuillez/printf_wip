/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nbr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/20 16:25:52 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 19:18:27 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void		ft_putnbr_base(unsigned long nb, char *base)
{
	if (nb / ft_strlen(base) != 0)
		ft_putnbr_base(nb / ft_strlen(base), base);
	ft_putchar(base[nb % ft_strlen(base)]);
}

void		ft_print_nbr_p(void *data, t_format *f)
{
	unsigned long	ulnb;
	static char		*base = "0123456789abcdef";

	ulnb = (unsigned long)data;
	if (f->w == 1 && f->width > 1)
		f->width -= 2;
	if (f->w == 1 && f->width < -1)
		f->width += 2;
	f->tot += write(1, "0x", 2);
	if (f->p == 1 && f->precision == 0 && data == 0)
		f->tot = f->tot;
	else
	{
		if (f->sp == 'p')
			ft_putnbr_base(ulnb, base);
		f->tot += ft_nb_length(ulnb, base);
	}
}

void		ft_print_nbr_o(void *data, t_format *f)
{
	unsigned int	unb;
	static char		*base = "01234567";

	unb = (unsigned int)data;
	if (f->p == 1 && f->precision == 0 && data == 0)
		f->tot = f->tot;
	else
	{
		if ((f->flag & DIESE) != 0 && data != 0)
			f->tot += write(1, "0", 1);
		ft_putnbr_base(unb, base);
		f->tot += ft_nb_length(unb, base);
	}
}

void		ft_print_nbr_x(void *data, t_format *f)
{
	unsigned int	unb;
	static char		*base = "0123456789abcdef";

	unb = (unsigned int)data;
	if (f->p == 1 && f->precision == 0 && data == 0)
		f->tot = f->tot;
	else
	{
		if (f->sp == 'x')
		{
			if ((f->flag & DIESE) != 0 && data != 0)
				f->tot += write(1, "0x", 2);
			ft_putnbr_base(unb, base);
		}
		if (f->sp == 'X')
		{
			if ((f->flag & DIESE) != 0 && data != 0)
				f->tot += write(1, "0X", 2);
			ft_putnbr_base(unb, base);
			ft_putnbr_base(unb, "0123456789ABCDEF");
		}
		f->tot += ft_nb_length(unb, base);
	}
}

void		ft_print_nbr_dui(void *data, t_format *f)
{
	unsigned int	unb;
	static char		*base = "0123456789";

	unb = (unsigned int)data;
	if ((f->sp == 'i' || f->sp == 'd') && (int)data < 0)
	{
		f->tot += write(1, "-", 1);
		if (f->p != 0 && f->precision == 0 && data == 0)
			f->tot--;
		else
		{
			ft_putnbr_base(-unb, base);
		}
		f->tot += ft_nb_length(-unb, base);
	}
	else
	{
		if (f->p != 0 && f->precision == 0 && data == 0)
			f->tot--;
		else
			ft_putnbr_base(unb, base);
		f->tot += ft_nb_length(unb, base);
	}
}
