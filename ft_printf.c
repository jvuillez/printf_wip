/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <jvuillez@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/05 15:38:58 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 19:18:26 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

int				ft_true_char(char c)
{
	static char	true_chars[] = "%pdDioOuUxXcCsS#-+ *.0123456789hljz";
	int			i;

	i = 0;
	while (true_chars[i] != '\0' && c != true_chars[i])
		i++;
	if (true_chars[i] == '\0')
		return (-1);
	return (i);
}

int				ft_specifier(char c)
{
	static char	specifiers[] = "%pdDioOuUxXcCsS";
	int			i;

	i = 0;
	while (specifiers[i] != '\0' && c != specifiers[i])
		i++;
	if (specifiers[i] == '\0')
		return (-1);
	return (i);
}

unsigned int	ft_strlen_next(char *str, unsigned int i)
{
	if (i == 1)
	{
		while ((str[i] != '\0') && \
				ft_true_char(str[i]) != -1 && \
				ft_specifier(str[i]) == -1)
			i++;
		if (str[i] == '\0')
			return (i);
		else
			return (i + 1);
	}
	else
	{
		while ((str[i] != '\0') && (str[i] != '%'))
			i++;
		return (i);
	}
}

t_elem			*ft_lexer(const char *format)
{
	unsigned int	i;
	t_elem			*start;
	t_elem			*tmp;

	start = malloc(sizeof(t_elem));
	tmp = start;
	while (ft_strlen((char*)format) != 0)
	{
		i = 0;
		if (*format == '%')
			i = 1;
		tmp->next = malloc(sizeof(t_elem));
		tmp->str = malloc(sizeof(char) * ft_strlen_next((char*)format, i));
		ft_strnclone((char*)format, tmp->str, ft_strlen_next((char*)format, i));
		format = format + ft_strlen_next((char*)format, i);
		tmp = tmp->next;
	}
	tmp->next = NULL;
	return (start);
}

int				ft_printf(const char *format, ...)
{
	int			tot;
	va_list		ap;
	t_elem		*start;
	t_elem		*tmp;

	tot = 0;
	va_start(ap, format);
	start = ft_lexer(format);
	tmp = start;
	while (tmp->next != NULL)
	{
		if (tmp->str[0] == '%')
			tot += ft_parser(tmp->str + 1, ap);
		else
		{
			ft_putstr(tmp->str);
			tot += ft_strlen(tmp->str);
		}
		tmp = tmp->next;
	}
	return (tot);
}
