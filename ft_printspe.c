/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printspe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/05 18:19:51 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 19:18:21 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

int			size_bin(wchar_t value)
{
	int		size;

	size = 0;
	while (value > 0)
	{
		value = value / 2;
		size++;
	}
	return (size);
}

void		ft_print_unicode(unsigned char *octet, unsigned int value)
{
	static unsigned int		mask[4] = {0, 49280, 14712960, 4034953344};

	if (value > 16)
	{
		octet[3] = (value << 26) >> 26;
		octet[2] = ((value >> 6) << 26) >> 26;
		octet[1] = ((value >> 12) << 26) >> 26;
		octet[0] = ((value >> 18) << 29) >> 29;
		octet[0] = (mask[3] >> 24) | octet[0];
		octet[1] = ((mask[3] << 8) >> 24) | octet[1];
		octet[2] = ((mask[3] << 16) >> 24) | octet[2];
		octet[3] = ((mask[3] << 24) >> 24) | octet[3];
	}
	write(1, &octet[0], 1);
	if (value > 7)
		write(1, &octet[1], 1);
	if (value > 11)
		write(1, &octet[2], 1);
	if (value > 16)
		write(1, &octet[3], 1);
}

void		ft_putwchar(wchar_t v)
{
	static unsigned int		mask[4] = {0, 49280, 14712960, 4034953344};
	static unsigned char	octet[4] = {0, 0, 0, 0};
	unsigned int			value;

	value = v;
	if (size_bin(value) <= 7)
		octet[0] = value;
	else if (size_bin(value) <= 11)
	{
		octet[1] = (value << 26) >> 26;
		octet[0] = ((value >> 6) << 27) >> 27;
		octet[0] = (mask[1] >> 8) | octet[0];
		octet[1] = ((mask[1] << 24) >> 24) | octet[1];
	}
	else if (size_bin(value) <= 16)
	{
		octet[2] = (value << 26) >> 26;
		octet[1] = ((value >> 6) << 26) >> 26;
		octet[0] = ((value >> 12) << 28) >> 28;
		octet[0] = (mask[2] >> 16) | octet[0];
		octet[1] = ((mask[2] << 16) >> 24) | octet[1];
		octet[2] = ((mask[2] << 24) >> 24) | octet[2];
	}
	ft_print_unicode(octet, size_bin(value));
}

void		ft_print_string(void *data, t_format *f)
{
	wchar_t		*wstr;
	wchar_t		w;
	int			max;

	if (data == NULL)
	{
		if (f->p == 0 || f->precision < 0)
			f->precision = 6;
		max = f->precision < 6 ? f->precision : 6;
		f->tot += (write(1, "(null)", max));
	}
	if (data != NULL)
	{
		if (f->p == 0 || f->precision < 0)
			f->precision = ft_strlen((char*)data);
		max = f->precision < ft_strlen((char*)data) ? \
				f->precision : ft_strlen((char*)data);
		if (f->sp == 's')
			f->tot += write(1, (char*)data, max);
		if (f->sp == 'S')
		{
			w = 0;
			wstr = (wchar_t*)data;
			while (wstr[w] != '\0')
				ft_putwchar(wstr[w++]);
			f->tot += (f->sp = ft_ustrlen((wchar_t*)data));
		}
	}
}

void		ft_print_char(void *data, t_format *f)
{
	char c;

	c = (char)data;
	if (f->sp == '%')
	{
		c = '%';
		f->tot += (write(1, &c, 1));
	}
	else if (f->sp == 'c')
	{
		if (f->length != NULL && f->length[0] == 'l' && f->length[1] == '\0')
		{
			ft_putwchar((wchar_t)data);
			f->tot += 1;
		}
		f->tot += write(1, &data, 1);
	}
	else
		ft_putwchar((wchar_t)data);
}

void		ft_init_tab_ft(void (*tab_ft[127])(void *data, t_format *f))
{
	tab_ft['p'] = &ft_print_nbr_p;
	tab_ft['d'] = &ft_print_nbr_dui;
	tab_ft['D'] = &ft_print_nbr_dui;
	tab_ft['i'] = &ft_print_nbr_dui;
	tab_ft['o'] = &ft_print_nbr_o;
	tab_ft['O'] = &ft_print_nbr_o;
	tab_ft['u'] = &ft_print_nbr_dui;
	tab_ft['U'] = &ft_print_nbr_dui;
	tab_ft['x'] = &ft_print_nbr_x;
	tab_ft['X'] = &ft_print_nbr_x;
	tab_ft['%'] = &ft_print_char;
	tab_ft['c'] = &ft_print_char;
	tab_ft['C'] = &ft_print_char;
	tab_ft['s'] = &ft_print_string;
	tab_ft['S'] = &ft_print_string;
}
