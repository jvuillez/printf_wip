/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools_common.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 13:53:20 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 14:01:46 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

void	ft_strnclone(char *src, char *dst, unsigned int n)
{
	while (n > 0)
	{
		*dst = *src;
		src++;
		dst++;
		n--;
	}
	*dst = '\0';
}

char	*ft_strcpy(char *str)
{
	char	*new;
	int		i;

	i = 0;
	new = malloc(sizeof(char) * ft_strlen(&str[i]));
	while (str[i] != '\0')
	{
		new[i] = str[i];
		i++;
	}
	return (new);
}

int		ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int		ft_atoi(const char *str)
{
	int	i;
	int	pn;

	i = 0;
	while (*str != '\0' && *str != '-' && *str != '+' && *str <= 32)
		str++;
	pn = 1;
	if (*str == '-')
		pn = -1;
	if (*str == '+' || *str == '-')
		str++;
	else if (ft_isdigit(*str != 1))
		return (0);
	while ((*str != '\0') && (ft_isdigit(*str) == 1))
	{
		i = i * 10 + *str - '0';
		str++;
	}
	return (i * pn);
}
