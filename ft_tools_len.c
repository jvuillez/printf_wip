/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools_len.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 13:53:29 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 13:53:37 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

int		ft_nb_length(unsigned long nb, char *base)
{
	int			base_len;
	int			nb_len;

	base_len = ft_strlen(base);
	nb_len = 0;
	if (nb == 0)
		nb_len = 1;
	while (nb != 0)
	{
		nb_len++;
		nb = nb / base_len;
	}
	return (nb_len);
}

int		ft_ustrlen(wchar_t *str)
{
	wchar_t	i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

int		ft_strlen(char *str)
{
	int		i;

	i = 0;
	if (str == NULL || *str == '\0')
		return (0);
	while (str[i] != '\0')
		i++;
	return (i);
}

int		ft_strlen_char(char *str, char c)
{
	int		i;

	i = 0;
	while (str[i] != '\0' && str[i] != c)
		i++;
	return (i);
}
