/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools_put.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 13:53:49 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 13:58:57 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libprintf.h"

int		ft_putnbr(int n)
{
	if (n == -2147483648)
		return (ft_putstr("-2147483648"));
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	if (n / 10)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else
		ft_putchar(n + 48);
	return (n);
}

void	ft_print_list(t_elem *start)
{
	t_elem	*tmp;

	tmp = start;
	while (tmp->next != NULL)
	{
		ft_putstr(tmp->str);
		ft_putchar('\n');
		tmp = tmp->next;
	}
}

void	ft_print_tab(char **tab)
{
	int		i;

	i = 0;
	while (tab[i] != NULL)
	{
		ft_putstr(tab[i]);
		ft_putchar('\n');
		i++;
	}
}

int		ft_spam(char c, int i)
{
	int		j;

	j = -1;
	while (++j != i)
		write(1, &c, 1);
	return (j);
}

int		ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
	return (ft_strlen(str));
}

int		ft_putchar(char c)
{
	write(1, &c, 1);
	return (1);
}
