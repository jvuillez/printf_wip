/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libprintf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <jvuillez@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/05 16:20:35 by jvuillez          #+#    #+#             */
/*   Updated: 2016/04/04 19:18:24 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBPRINTF_H
# define LIBPRINTF_H

# define MOINS 1
# define PLUS 2
# define SPACE 4
# define DIESE 8
# define ZERO 16

# define START 0
# define MIDDLE 1
# define END 2

# include <locale.h>
# include <wchar.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <stdbool.h>

typedef struct		s_elem{
	char			*str;
	struct s_elem	*next;
}					t_elem;

typedef struct		s_format{
	bool			f;
	int				flag;
	bool			w;
	int				width;
	bool			p;
	int				precision;
	char			*length;
	int				sp;
	unsigned int	tot;
}					t_format;

/*
** ft_put_tools.c
*/

int					ft_spam(char c, int i);
int					ft_putnbr(int n);
int					ft_putchar(char c);
int					ft_putstr(char *str);
void				ft_print_tab(char **tab);
void				ft_print_list(t_elem *start);
void				ft_print_unicode(unsigned char *octet, unsigned int value);

/*
** ft_str_tools.c
*/

int					ft_strlen(char *str);
int					ft_ustrlen(wchar_t *str);
void				ft_strnclone(char *src, char *dst, unsigned int n);
char				*ft_strcpy(char *str);
int					ft_strlen_char(char *str, char c);
int					ft_nb_length(unsigned long nb, char *base);

/*
** ft_common_tools.c
*/

int					ft_isdigit(int c);
int					ft_atoi(const char *str);

/*
** ft_parser.c
*/

int					ft_flags(char *str, t_format *f);
int					ft_width(char *str, t_format *f, va_list ap);
int					ft_precision(char *str, t_format *f, va_list ap);
int					ft_length(char *str, t_format *f);
int					ft_parser(char *str, va_list ap);

/*
** ft_printf.c
*/

int					ft_specifier(char c);
unsigned int		ft_strlen_next(char *str, unsigned int i);
t_elem				*ft_lexer(const char *format);
int					ft_printf(const char *format, ...);

/*
** ft_printspe.c
*/

void				ft_print_string(void *data, t_format *f);
void				ft_print_char(void *data, t_format *f);
void				ft_init_tab_ft(void (*tab_ft[127])(void *data, t_format *f));

/*
** ft_print_nbr.c
*/

void				ft_putnbr_base(unsigned long nb, char *base);
void				ft_print_nbr_p(void *data, t_format *f);
void				ft_print_nbr_o(void *data, t_format *f);
void				ft_print_nbr_x(void *data, t_format *f);
void				ft_print_nbr_dui(void *data, t_format *f);

#endif
